#!/bin/bash

native_dir=/work/ka1081/2019_06_Hackathon_Mainz/christoph_heim/newdata/FV3_3.25/tmp/10days

var_names=(H T QV QC W U V\
           U10M V10M T2M LWUTOA SWDSFC SWUTOA SWDTOA TQC TQI PP SLHFLX SSHFLX )
var_names=(U V CLCT PS TQV)

times=(20160801 20160811 20160821 20160831)

ls $native_dir

for var in  ${var_names[@]}; do
    echo $var
    for time in  ${times[@]}; do
        echo '     '$time
        inp_file=$native_dir/${var}_${time}0000.nc
        #echo $inp_file
        if [ $time == "20160831" ]; then
            out_base=$native_dir/../${var}_2016
            cdo splitmon $inp_file $out_base
            cdo splitday ${out_base}08.nc ${out_base}08
            rm ${out_base}08.nc
            cdo splitday ${out_base}09.nc ${out_base}09
            rm ${out_base}09.nc
        else
            out_base=$native_dir/../${var}_201608
            #echo $out_base
            cdo splitday $inp_file $out_base
        fi
    
    done
done



