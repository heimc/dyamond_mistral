#!/bin/bash

# get tar files from tape using
# pftp (& login without username)
# go to /hpss/arch/gg0302/g266006/reanalyses/ERA5

# (terrible) function to return next month 
month_next_month () {
    if   [ $1 -lt 9 ]; then 
        var=$(expr $1 + 1)
        echo 0$var
    elif [ $1 -eq 9 ]; then 
        echo 10
    elif [ $1 -eq 12 ]; then 
        echo 01
    elif [ $1 -gt 9 ]; then 
        echo $(($1 + 1))
    fi
}
# (terrible) function to return year of next month 
year_next_month () {
    if   [ $2 -lt 12 ]; then 
        echo $1
    elif [ $2 -eq 12 ]; then 
        echo $(($1 + 1))
    fi
}

# Atlantic DOMAIN (heimc)
lon0=-71.0
lon1=27.0
lat0=-32.0
lat1=32.0
start_lev=30
domain=heimc

## Asian DOMAIN (Ruolan)
#lon0=30.
#lon1=-160.
#lat0=-15.
#lat1=75.
#start_lev=25
#domain=ruolan

#base_dir=/work/ka1081/2019_06_Hackathon_Mainz/christoph_heim/extract_ERA5
base_dir=/scratch/b/b380876
full_dom_dir=$base_dir/full_domain/$domain
sub_dom_dir=$base_dir/subdomain/$domain
packed_dom_dir=$base_dir/packed/$domain
daint_dir=heimc@daint.cscs.ch:/store/c2sm/c2sme/ERA5_int2lmready/$domain

parts=(1 2 3 4 5 6 7 8)

year=2007
year=2008
year=2009
year=2010
year=2017
year=2018
year=2019

months=(01 02 03 04 05 06 07 08 09 10 11 12)
#months=(06 07 08 09 10 11 12)
#months=(03 04 05 06 07 08)
#months=(12)
months=(01 03 04 05 06 07 08 09 10 11 12)
months=(01 03 04 05 06 07 08 09 10 11)

#hrs=(00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23)
hrs=(00 03 06 09 12 15 18 21)
#hrs=(00 06 12 18)

days=(01 02 03 04 05 06 07 08 09 10 \
      11 12 13 14 15 16 17 18 19 20 \
      21 22 23 24 25 26 27 28 29 30 31)

i_download=1
i_subdomain=1
i_tarball=1
i_copy_daint=1


echo $year
module add packems
tapeinit
for month in ${months[@]}; do

    echo "###############################################################################"
    echo "###############################################################################"
    echo "###############################################################################"
    echo "###############################################################################"
    echo start $domain $year $month
    echo "###############################################################################"
    echo "###############################################################################"
    echo "###############################################################################"
    echo "###############################################################################"

    ### MAKE DIRECTORIES
    ###################################################
    mkdir -p $full_dom_dir/$year/$month
    mkdir -p $sub_dom_dir/$year/$month
    mkdir -p $packed_dom_dir/$year/$month

    ### DOWNLOAD FILES FROM TAPE
    ###################################################
    if [ $i_download == 1 ]; then
    for part in ${parts[@]}; do
        echo "############################################################################"
        echo download $year $month part $part from tape
        echo "############################################################################"
        hpss_era5_path=t:/hpss/arch/gg0302/g266006/reanalyses/ERA5
        unpackems -p -O -A \
                "${hpss_era5_path}/year${year}/ERA5_${year}_${month}_part${part}.tar" \
                -d $full_dom_dir/$year/$month -w $full_dom_dir/$year/$month/work
    done
    fi

    #### UNTAR FILES (not necessary thanks to unpackems)
    ####################################################
    #mkdir -p $full_dom_dir/$year
    #    for part in ${parts[@]}; do
    #        echo $part
    #        tar -xf $base_dir/$year/ERA5_${year}_${month}_part${part}.tar \
    #                    -C $full_dom_dir/$year
    #    done
    ####exit 1


    ###### CUT OUT SUBDOMAIN AND RENAME FILES
    ######################################################
    if [ $i_subdomain == 1 ]; then
    for day in ${days[@]}; do
        echo $year$month$day
        for hr in ${hrs[@]}; do
        
            ## extract subdomain
            ncks -O -F  -d level1,$start_lev,138 -d level,$start_lev,137 \
                        -d lon,$lon0,$lon1 -d lat,$lat0,$lat1 \
                        $full_dom_dir/$year/$month/caf${year}${month}${day}${hr}.nc \
                        $sub_dom_dir/$year/$month/cas${year}${month}${day}${hr}0000.nc
                        #$full_dom_dir/$year/$month/cas$year${month}${day}${hr}0000.nc \
                        #$sub_dom_dir/$year/$month/cas$year${month}${day}${hr}0000.nc
        
            ## correct soil1 layer
            python aux_pp_ERA5.py \
                $sub_dom_dir/$year/$month/cas${year}${month}${day}${hr}0000.nc 
        
        done
    done

    # repeat for first hour of next month
    ncks -O -F  -d level1,$start_lev,138 -d level,$start_lev,137 \
                -d lon,$lon0,$lon1 -d lat,$lat0,$lat1 \
                $full_dom_dir/$year/$month/caf$(year_next_month $year $month)$(month_next_month $month)0100.nc \
                $sub_dom_dir/$year/$month/cas$(year_next_month $year $month)$(month_next_month $month)01000000.nc
    python aux_pp_ERA5.py \
            $sub_dom_dir/$year/$month/cas$(year_next_month $year $month)$(month_next_month $month)01000000.nc 
    fi



    #### PACK FILES
    ####################################################
    # jump to file input directory to prevent .tar files to
    # contain folder structure.
    # (could surely also be prevented in another way)
    if [ $i_tarball == 1 ]; then
    cd $sub_dom_dir/$year/$month
        # create tar-ball
        tar -s -cvf $packed_dom_dir/$year/$month/ERA5_cas_${year}_${month}.tar cas* 
                #cas${year}${month}* cas${year}$(month_next_month $month)01000000.nc
    cd -
    fi


    ### COPY FILES TO DAINT
    ###################################################
    if [ $i_copy_daint == 1 ]; then
    echo copy $year $month to daint
    rsync -u $packed_dom_dir/$year/$month/ERA5_cas_${year}_${month}.tar \
                $daint_dir/${year}/
    fi


    echo "###############################################################################"
    echo "###############################################################################"
    echo "###############################################################################"
    echo "###############################################################################"
    echo completed $year $month
    echo "###############################################################################"
    echo "###############################################################################"
    echo "###############################################################################"
    echo "###############################################################################"
done

#### SUMMARY
####################################################
echo ${months[@]}
echo $domain $year
#/hpss/arch/gg0302/g266006/reanalyses/ERA5
