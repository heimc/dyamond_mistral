#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description:    Extract lat-lon box of data from model ICON.
author:         Christoph Heim
date created:   27.06.2019
date changed:   27.06.2019
usage:          arguments:
                1st:    n jobs for multiprocessing pool
python:         3.5.2
"""
###############################################################################
import os, glob, subprocess, sys, time
import numpy as np
from datetime import datetime, timedelta
from multiprocessing import Pool
from pathlib import Path
from cdo import Cdo
from utilities import Timer
###############################################################################

def sellatlon_ICON(inp_file, out_file, grid_def, dt, box, i_recompute):
    
    TM = Timer()
    TM.start('total')

    ofile = cdo.gennn('latlon_1_deg', input=(' -setgrid,'+grid_def+
                           ' '+inp_file), output='weights_test')
    quit()
    
    if os.path.exists(out_file) and not i_recompute:
        print('\t\t{:%Y%m%d%H} already computed -> skip'.format(dt))
        TM.start('cdo')
        TM.stop('cdo')
    else:


        # cdo
        TM.start('cdo')
        #print('\t{:%Y%m%d%H} cdo'.format(dt))
        ofile = cdo.sellonlatbox(
                    box['lon0'],box['lon1'],
                    box['lat0'],box['lat1'],
                    input=('-sellevidx,'+str(box['vert0'])+'/'+
                           str(box['vert1'])+' -setgrid,'+grid_def+
                           ' '+inp_file),
                    output=out_file)

        print('\t\t{:%Y%m%d%H} completed'.format(dt))
        TM.stop('cdo')

    TM.stop('total')

    return(TM)


if __name__ == '__main__':

    # GENERAL SETTINGS
    ###########################################################################
    # input and output directories
    raw_data_dir = os.path.join('/work','ka1081','DYAMOND')
    out_base_dir = os.path.join('/work','ka1081','2019_06_Hackathon_Mainz',
                                'christoph_heim','data')

    # lat lon vert box to subselect
    #box = {'lon0': 270, 'lon1': 280, 'lat0': -24, 'lat1': -14,
    #        'vert0':1,'vert1':18}
    #box = {'lon0': 270, 'lon1': 271, 'lat0': -24, 'lat1': -23,
    #        'vert0':1,'vert1':18}
    box = {'lon0': 265, 'lon1': 281, 'lat0': -24, 'lat1': -14,
           'vert0':1,'vert1':18}

    # name of model 
    model_name = 'ICON'

    # variables to extract
    var_names = ['QC', 'T']
    #var_names = ['QC']
    
    # model resolutions [km] of simulations
    ress = [10,5,2.5]
    #ress = [5]
    
    # date range
    first_date = datetime(2016,8,10)
    last_date = datetime(2016,8,10)

    # recompute cdo
    i_recompute = 0
    ###########################################################################


    # ICON SPECIFIC SETTINGS
    ###########################################################################
    grid_def_base_dir = os.path.join('/work','bk1040','experiments', 'input')
    var_dict = {
        'QC':{'file':'tot_qc_dia',}, 
        'T':{'file':'t',}, 
    }
    grid_dict = {
        5:  {'grid_def':os.path.join(grid_def_base_dir,
                        '5km_2/icon_grid_0015_R02B09_G.nc')}, 
        2.5:{'grid_def':os.path.join(grid_def_base_dir,
                        '2.5km/icon_grid_0017_R02B10_G.nc')}, 
    }
    ###########################################################################

    ## PREPARING STEPS
    TM = Timer()
    TM.start('real')

    dt_range = np.arange(first_date, last_date + timedelta(days=1),
                        timedelta(days=1)).tolist()

    cdo = Cdo()

    if len(sys.argv) > 1:
        n_tasks = int(sys.argv[1])
    else:
        n_tasks = 1
    print('Using ' + str(n_tasks) + ' taks.')

    ## EXTRACT VARIABLES FROM SIMULATIONS
    for var_name in var_names:
        #var_name = 'T'
        print('############## var ' + var_name + ' ##################')
        for res in ress:
            print('############## res ' + str(res) + ' ##################')
            #res = 4

            sim_name = model_name + '-' + str(res) + 'km'
            inp_dir	= os.path.join(raw_data_dir, sim_name)
            # directory for final model output (after mergetime)
            out_dir = os.path.join(out_base_dir, model_name + '_' + str(res))
            Path(out_dir).mkdir(parents=True, exist_ok=True)
            # directory for output of files in time merge level of raw model
            # output
            out_tmp_dir = os.path.join(out_base_dir, model_name + 
                                       '_' + str(res),'tmp')
            Path(out_tmp_dir).mkdir(parents=True, exist_ok=True)

            # find times and files that should be extracted
            # and prepare arguments for function
            args = []
            for dt in dt_range:
                inp_files_glob = glob.glob(os.path.join(inp_dir,
                                            '*_{}_*{:%Y%m%d}*'.format(
                                            var_dict[var_name]['file'], dt)))
                inp_file = os.path.join(inp_files_glob[0])

                out_file = os.path.join(out_tmp_dir,
                            var_name+'_{:%Y%m%d}'.format(dt)+'.nc')
                grid_def = grid_dict[res]['grid_def']
                args.append( (inp_file, out_file, grid_def,
                              dt, box, i_recompute) )

            # run function serial or parallel
            if n_tasks > 1:
                with Pool(processes=n_tasks) as pool:
                    results = pool.starmap(sellatlon_ICON, args)
            else:
                results = []
                for arg in args:
                    results.append(sellatlon_ICON(*arg))

    # collect timings from subtasks
    for task_TM in results:
        TM.merge_timings(task_TM)

    TM.stop('real')
    TM.print_report()
