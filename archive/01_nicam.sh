#!/bin/bash

out_base='/work/ka1081/2019_06_Hackathon_Mainz/christoph_heim/data'

#lon0=-90
#lon1=-80
#lat0=-24
#lat1=-14

lon0=-90
lon1=-89
lat0=-24
lat1=-23

var_names=(ms_qc ms_qv ms_tem)
var_names=(ms_qc)

ress=(3.5 7)
ress=(7)

source ~/config

month=08
days=(11 12 13 14 15 16 17 18 19 20)
#inds=(0 1 2 3 4 5 6 7 8 9)

model_name=NICAM-${res}km
model_path=$store/${box_type}/data/$model_name
mkdir -p $model_path
cd $datad/$model_name


for ind in ${inds[@]} ; do
  dt0=2016${months[$ind]}${days[$ind]}
  day1=${days[$ind]}
  dt1=2016${months[$ind]}$(( $day1 + 1))
  echo $dt0

  mkdir -p $model_path/${dt0}

  cd ${dt0}.000000-${dt1}.000000
  cdo -P $njobs -sellonlatbox,-90,-80,-24,-14 \
                -sellevidx,1/18 \
                ${var_name}.nc $model_path/$dt0/${var_name}.nc
  cd ../
done
