#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description:    Extract lat-lon box of data from model SAM.
author:         Christoph Heim
date created:   20.06.2019
date changed:   27.06.2019
usage:          arguments:
                1st:    n jobs for multiprocessing pool
python:         3.5.2
"""
###############################################################################
import os, glob, subprocess, sys, time
import numpy as np
from datetime import datetime, timedelta
from multiprocessing import Pool
from pathlib import Path
from cdo import Cdo
from utilities import Timer
###############################################################################

def sellatlon_SAM(inp_file, out_file, dt, box, i_recompute):
    
    TM = Timer()
    TM.start('total')
    
    if os.path.exists(out_file) and not i_recompute:
        print('\t\t{:%Y%m%d%H} already computed -> skip'.format(dt))
        TM.start('nco')
        TM.stop('nco')
        TM.start('cdo')
        TM.stop('cdo')
    else:


        split = os.path.split(out_file)
        nco_file = os.path.join(split[0],'nco_'+split[1])

        # nco
        if not os.path.exists(nco_file):
            TM.start('nco')
            #print('\t{:%Y%m%d%H} nco'.format(dt))
            bash_command = ('ncatted -O -a units,lon,o,c,degrees_east ' + 
                            '-a units,lat,o,c,degrees_north '+inp_file+
                            ' '+nco_file)
            process = subprocess.Popen(bash_command.split(),
                                        stdout=subprocess.PIPE)
            output, error = process.communicate()
            TM.stop('nco')
        else:
            TM.start('nco')
            TM.stop('nco')

        # cdo
        TM.start('cdo')
        #print('\t{:%Y%m%d%H} cdo'.format(dt))
        ofile = cdo.sellonlatbox(
                    box['lon0'],box['lon1'],
                    box['lat0'],box['lat1'],
                    input=('-sellevidx,'+str(box['vert0'])+'/'+
                           str(box['vert1'])+' -settaxis,'+
                           '{:%Y-%m-%d,%H:%M:%S,3hour}'.format(dt)+
                          ' '+nco_file),
                    output=out_file)

        print('\t\t{:%Y%m%d%H} completed'.format(dt))
        TM.stop('cdo')

    TM.stop('total')

    return(TM)


if __name__ == '__main__':

    # GENERAL SETTINGS
    ###########################################################################
    # input and output directories
    raw_data_dir = os.path.join('/work','ka1081','DYAMOND')
    out_base_dir = os.path.join('/work','ka1081','2019_06_Hackathon_Mainz',
                                'christoph_heim','data')

    # lat lon vert box to subselect
    #box = {'lon0': 270, 'lon1': 280, 'lat0': -24, 'lat1': -14,
    #        'vert0':1,'vert1':28}
    box = {'lon0': 265, 'lon1': 281, 'lat0': -24, 'lat1': -14,
           'vert0':1,'vert1':28}

    # name of model 
    model_name = 'SAM'

    # variables to extract
    var_names = ['QC', 'T']
    #var_names = ['QC']
    
    # model resolutions [km] of simulations
    ress = [4]
    
    # date range
    first_date = datetime(2016,8,10)
    last_date = datetime(2016,8,31)

    # recompute cdo
    i_recompute = 0
    ###########################################################################


    # SAM SPECIFIC SETTINGS
    ###########################################################################
    var_dict = {
        'QC':{'file':'QC',
              'loc':'OUT_3D','fntime':(-16,-6),}, 
        'T':{'file':'TABS',
              'loc':'OUT_3D','fntime':(-18,-8),}, 
    }

    dt = 7.5
    base_time = datetime(2016,8,1)
    ###########################################################################

    ## PREPARING STEPS
    TM = Timer()
    TM.start('real')
    cdo = Cdo()

    if len(sys.argv) > 1:
        n_tasks = int(sys.argv[1])
    else:
        n_tasks = 1
    print('Using ' + str(n_tasks) + ' taks.')

    ## EXTRACT VARIABLES FROM SIMULATIONS
    for var_name in var_names:
        #var_name = 'T'
        print('############## var ' + var_name + ' ##################')
        for res in ress:
            print('############## res ' + str(res) + ' ##################')
            #res = 4

            sim_name = model_name + '-' + str(res) + 'km'
            inp_dir	= os.path.join(raw_data_dir, sim_name,
                                   var_dict[var_name]['loc'])
            # directory for final model output (after mergetime)
            out_dir = os.path.join(out_base_dir, model_name + '_' + str(res))
            Path(out_dir).mkdir(parents=True, exist_ok=True)
            # directory for output of files in time merge level of raw model
            # output
            out_tmp_dir = os.path.join(out_base_dir, model_name + 
                                       '_' + str(res),'tmp')
            Path(out_tmp_dir).mkdir(parents=True, exist_ok=True)

            # find times and files that should be extracted
            inp_files_glob = glob.glob(os.path.join(inp_dir,
                                        '*_'+var_dict[var_name]['file']+'.nc'))
            times = [base_time + timedelta(seconds=dt*int(
                        f[var_dict[var_name]['fntime'][0]:
                          var_dict[var_name]['fntime'][1]])) 
                        for f in inp_files_glob]
            use_times = [dt for dt in times if dt >= first_date and
                                           dt < last_date+timedelta(days=1)]
            use_files = [inp_files_glob[i] 
                        for i in range(len(inp_files_glob)) if 
                         times[i] in use_times]

            # prepare arguments for function
            args = []
            for i in range(len(use_times)):
                inp_file = use_files[i]
                out_file = os.path.join(out_tmp_dir,
                            var_name+'_{:%Y%m%d%H}'.format(use_times[i])+'.nc')
                args.append( (inp_file, out_file, use_times[i], box,
                              i_recompute) )

            # run function serial or parallel
            if n_tasks > 1:
                with Pool(processes=n_tasks) as pool:
                    results = pool.starmap(sellatlon_SAM, args)
            else:
                results = []
                for arg in args:
                    results.append(sellatlon_SAM(*arg))

    # collect timings from subtasks
    for task_TM in results:
        TM.merge_timings(task_TM)

    TM.stop('real')
    TM.print_report()
