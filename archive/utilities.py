#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
description:    Useful stuff
author:         Christoph Heim
date created:   27.06.2019
date changed:   27.06.2019
usage:          arguments:
python:         3.5.2
"""
###############################################################################
import time
import numpy as np
###############################################################################

class Timer:
    
    def __init__(self):

        self.timings = {}
        self.flags = {}

    def start(self, timer_key):
        if timer_key not in self.timings.keys():
            self.timings[timer_key] = 0.
            self.flags [timer_key] = None
        self.flags[timer_key] = time.time()

    def stop(self, timer_key):
        if (timer_key not in self.flags.keys()
            or self.flags[timer_key] is None):
            raise ValueError('No time measurement in progress for timer ' +
                            str(timer_key) + '.')

        self.timings[timer_key] += time.time() - self.flags[timer_key]
        self.flags[timer_key] = None

    def merge_timings(self, Timer):
        for timer_key in Timer.flags.keys():
            if timer_key in self.timings.keys():
                self.timings[timer_key] += Timer.timings[timer_key]
            else:
                self.timings[timer_key] = Timer.timings[timer_key]

    def print_report(self):
        n_decimal_perc = 0
        n_decimal_sec = 1
        n_decimal_min = 2
        comp_time = self.timings['total']
        real_time = self.timings['real']
        print('############################################################')
        print('Took ' + str(np.round(real_time/60,n_decimal_min)) + ' min.')
        print('Detailed process times (cpu time, not real time):')
        for key,value in self.timings.items():
            print(key + '\t' + 
                str(np.round(100*value/comp_time,n_decimal_perc)) +
            '\t%\t' + str(np.round(value/60,n_decimal_min)) + ' \tmin')

