import os
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import xarray as xr
import numpy as np
from cdo import Cdo


if __name__ == '__main__':

    base_path_sh = '/work/ka1081/2019_06_Hackathon_Mainz/shweta'
    base_path_ch = '/work/ka1081/2019_06_Hackathon_Mainz/christoph_heim'

    models = {
        'ICON':{
            'bp':base_path_sh,
            'res':[80,40,20,10,5,2.5],
            #'res':[10,5],
            'var_key':{'QV':'q', 'QC':'param212.1.0', 'T':'t'},
            'var_file':{'QV':'qv', 'QC':'tot_qc_dia', 'T':'t'},
            'vcoord':'height',
        },
        'NICAM':{
            'bp':base_path_ch,
            'res':[7,3.5],
            'var_key':{'QV':'q', 'QC':'ms_qc', 'T':'ms_tem'},
            'var_file':{'QV':'qv', 'QC':'ms_qc', 'T':'ms_tem'},
            'vcoord':'lev',
        },
        'SAM':{
            'bp':base_path_ch,
            'res':[4],
            'var_key':{'QV':'QV', 'QC':'QC', 'T':'TABS'},
            'var_file':{'QV':'QV', 'QC':'QC', 'T':'TABS'},
            'vcoord':'z',
        },
    }
    cdo = Cdo()

    var_name = 'T'
    #var_name = 'QC'
    #dt0 = datetime(2016,8,11)
    #dt1 = datetime(2016,8,12)

    dt_range = [datetime(2016,8,11), datetime(2016,8,12), 
                datetime(2016,8,13), datetime(2016,8,14)]
    #dt_range = [datetime(2016,8,13)]
    
    fig,axes = plt.subplots(2,2, figsize=(12,12))
    
    for ti,dt in enumerate(dt_range):
        ri = ti % 2
        ci = (ti - ri) / 2

        handles = []
        labels = []
        for mi,mk in enumerate(models.keys()):
            #mk = 'ICON'
            print(mk)
            for res in models[mk]['res']:
                #res = models[mk]['res'][0]
                #res = 80
                model_name = mk + '-' + str(res) + 'km'

                dt_str = str(dt.year) + str(dt.month) + str(dt.day)
                dt_str = '{:%Y%m%d}'.format(dt)
                file_name = os.path.join(models[mk]['bp'], 'SCu', 'data', 
                                    model_name, dt_str,
                                    models[mk]['var_file'][var_name] + '.nc')
                print(file_name)
                #field = cdo.fldmean(input=file_name, returnXArray='field')
                #dataset = xr.open_dataset(cdo.fldmean(input=file_name,
                #                                        options='-f nc'))
                dataset = xr.open_dataset(cdo.fldmean(input=file_name,
                                                        options='-f nc'))
                #dataset[models[mk]['var_key'][var_name]].plot()
                # TODO: show to Ralf
                #field = dataset[models[mk]['var_key'][var_name]]
                #print(field)
                #try:
                if mk == 'ICON':
                    #kind = 18
                    height = dataset.coords[models[mk]['vcoord']].values[-18:]
                    time = dataset.coords['time'].values[-18:]
                    values = dataset[models[mk]['var_key'][var_name]].\
                                    mean('time').values.squeeze()[-18:]
                else:
                    height = dataset.coords[models[mk]['vcoord']].values
                    time = dataset.coords['time'].values
                    values = dataset[models[mk]['var_key'][var_name]].\
                                    mean('time').values.squeeze()
                if mk == 'SAM':
                    values *= 0.001
                dataset.close()
                #except:
                #    pass

                ## fixes that are not nice..
                #print(height)
                #print(len(height))
                #print(np.arange(-len(height),-1,0))
                #quit()
                if mk == 'ICON':
                    ICON_hgt = np.loadtxt('ICON_hgt.txt')
                    ICON_hgt = ICON_hgt[:-1] + np.diff(ICON_hgt)/2
                    height = ICON_hgt[-len(height):]
                print(len(height))

                print(values.shape, height.shape)
                label = mk + '_' + str(res)
                labels.append(label)
                ax = axes[ri,ci]
                line, = ax.plot(values,height, label=label)
                #line, = ax.plot(1000*values,height, label=label)
                handles.append(line)
                ax.set_title('{:%Y-%m-%d}'.format(dt))
                ax.set_ylim(0,3000)
                ax.set_xlim(280,305)
                ax.set_xlabel('Temperature [K]')
                #ax.set_xlabel('QC [g kg-1]')
                ax.set_ylabel('Height [m]')
    #fig.legend(handles=handles, labels=labels, bbox_to_achnor=[0.9,0.9])
    plt.legend(handles=handles, fontsize=9)
    #fig.subplots_adjust(right=0.9)
    plt.show()
    #plt.savefig('qc_prof.pdf', format='pdf')
    #plt.savefig('temp_prof.pdf', format='pdf')
    #plt.close(fig.number)


