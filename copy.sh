#!/bin/bash

## SOURCE
# mistral
store=/work/ka1081/2019_06_Hackathon_Mainz/christoph_heim/newdata
# daint
#store=/project/pr04/heimc/data/dyamond
## DESTINATION
## o3
#dest_dir=heimc@fog.ethz.ch:/net/o3/hymet_nobackup/heimc/data/dyamond
# ela
dest_dir=heimc@ela.cscs.ch:/project/pr04/heimc/data/dyamond

#domain=N_Atl_Sc
#domain=SE_Pac_Sc
domain=DYAMOND_2

models=(GEOS_3 ICON_10 ICON_2.5 IFS_4 IFS_9 MPAS_3.75 MPAS_7.5 \
        NICAM_3.5 NICAM_7 SAM_4 UM_5 ARPEGE-NH_2.5 FV3_3.25)

models=(NICAM_7 NICAM_3.5)
models=(SAM_4)
models=(ICON_10 ICON_2.5)
#models=(UM_5)
#models=(MPAS_7.5 MPAS_3.75)
#models=(IFS_4 IFS_9)
#models=(GEOS_3)
#models=(ARPEGE-NH_2.5)
#models=(FV3_3.25)
#models=(COSMO_12)

##########################################################################

for model in ${models[@]}; do
    echo $model
    orig_path=$store/$model/$domain
    dest_path=$dest_dir/$model/$domain
    echo $orig_path
    echo $dest_path
    scp -r $orig_path $dest_path 
done

##########################################################################

#var_name=T
#for model in ${models[@]}; do
#    echo $model
#    #orig_path=$store/newdata/$model/$domain/${var_name}.nc
#    #dest_path=$o3_dir/$model/$domain
#    orig_path=$store/$model/$domain
#    dest_path=$o3_dir/$model
#    scp -r $orig_path $dest_path 
#done

##########################################################################

#var_name=LWUTOA
#for model in ${models[@]}; do
#    echo $model
#    orig_path=$store/$model/$domain/${var_name}.nc
#    dest_path=$o3_dir/$model/$domain
#    scp -r $orig_path $dest_path 
#done

##########################################################################
