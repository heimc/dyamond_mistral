import sys
import numpy as np
from netCDF4 import Dataset


file_path = sys.argv[1]

#print(file_path)

ds = Dataset(file_path, 'a')
ds['soil1'][-1] = 1.945

new_soil1_bnds = ds['soil1_bnds'][-1] 
new_soil1_bnds[1] = 2.89
ds['soil1_bnds'][-1] = new_soil1_bnds
ds.close()
